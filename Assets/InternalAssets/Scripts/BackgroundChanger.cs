﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class BackgroundChanger : MonoBehaviour
{
    public static BackgroundChanger Instance = null;

    public List<Material> backgroundMaterials;

    private int _randomBackgroundNumber;
    private int _randomBackgroundCount;

    private void Start()
    {
        _randomBackgroundNumber = Random.Range(0, backgroundMaterials.Count);
        SceneManager.sceneLoaded += SetChoosenSkybox;
        RenderSettings.skybox = backgroundMaterials[_randomBackgroundNumber];
    }

    private void SetChoosenSkybox(Scene arg0, LoadSceneMode arg1)
    {
        _randomBackgroundCount++;
        if (_randomBackgroundCount % 2 == 0)
            _randomBackgroundNumber = Random.Range(0, backgroundMaterials.Count);
        RenderSettings.skybox = backgroundMaterials[_randomBackgroundNumber];
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}