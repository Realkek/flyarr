﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine;
using UnityEngine.UI;

public class PlayGamesServicesInitializer : MonoBehaviour, IEventTrigger
{
    public static PlayGamesServicesInitializer Instance = null;

    // Start is called before the first frame update
    void Start()
    {
        // If there is not already an instance of AudioPlayer, set it to this.
        if (Instance == null)
        {
            Instance = this;
            AuthenticateUser();
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            });
            DontDestroyOnLoad(gameObject);
        }
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }


    void AuthenticateUser()
    {
        // Create client configuration
        // PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        // Enable debugging output (recommended)
        StartCoroutine(SignIn());
        // Initialize and activate the platform
        // PlayGamesPlatform.InitializeInstance(config);
        // Try silent sign-in (second parameter is isSilent)
        // PlayGamesPlatform.Instance.Authenticate(SignInCallback, true);
    }

    private IEnumerator SignIn()
    {
        yield return new WaitForSeconds(1f);
        if (!Social.localUser.authenticated)
        {
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                    TriggerEvent("GPGSUserAuthenticated");
            });
        }
    }

    public void GetAchieve(string id)
    {
        Social.ReportProgress(id, 100.0f, (bool success) =>
        {
            if (success) print("achieve received" + id);
        });
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}