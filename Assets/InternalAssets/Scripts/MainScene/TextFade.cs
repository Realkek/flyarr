﻿using UnityEngine;
using UnityEngine.UI;

public class TextFade : MonoBehaviour
{
    private Text m_FadeTxt;
    private Outline m_OLine;

    private void Start()
    {
        m_FadeTxt = GetComponent<Text>();
        m_OLine = GetComponent<Outline>();
    }

    // Update is called once per frame
    private void Update()
    {
        var color = m_FadeTxt.color;
        color = new Color(color.r, color.g, color.b, Mathf.PingPong(Time.time / 2.5f, 1.0f));
        m_FadeTxt.color = color;
        var effectColor = m_OLine.effectColor;
        effectColor = new Color(effectColor.r, effectColor.g, effectColor.b, m_FadeTxt.color.a -0.3f);
        m_OLine.effectColor = effectColor;
    }
}