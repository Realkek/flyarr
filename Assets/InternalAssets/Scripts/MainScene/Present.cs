﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Present : Buttons, IEventSub
{
    [SerializeField] private GameObject rewardsCanvas;
    [SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject mainArrow;
    [SerializeField] private GameObject rewardsContent;
    private bool _isInternetConnectionAccess;

    private void Start()
    {
        StartCoroutine(CheckInternetConnection());
    }

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        rewardsCanvas.SetActive(true);
        rewardsCanvas.GetComponent<Canvas>().enabled = true;
        mainScreen.SetActive(false);
        mainArrow.SetActive(false);
        ResetButtonSize();
    }

    private void CheckRewardsContent()
    {
        int activeRewardCanvasCount = 0;
        for (int i = 0; i < rewardsContent.transform.childCount; i++)
        {
            if (rewardsContent.transform.GetChild(i).gameObject.activeSelf)
            {
                activeRewardCanvasCount++;
            }
        }

        if (activeRewardCanvasCount == 0)
            gameObject.SetActive(false);
    }

    private IEnumerator CheckInternetConnection()
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            _isInternetConnectionAccess = false;
            gameObject.SetActive(false);
        }
        else
        {
            _isInternetConnectionAccess = true;
        }

        CheckRewardsContent();
        Subscribe();
    }

    public void GoShake()
    {
        GetComponent<Animation>().Play("ShakingPresent");
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("DailyRewardReceived", CheckRewardsContent);
    }

    public void UnSubscribe()
    {
        StopListening("DailyRewardReceived", CheckRewardsContent);
    }
}