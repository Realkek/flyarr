﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class PurchaseNoAds : MonoBehaviour, IEventTrigger
{
    public void OnPurchaseComplete(Product product)
    {
        if (product.definition.id == "flyarr_noads") PlayerPrefs.SetInt("flyarr_noads", 1);

        TriggerEvent("NoAdsPurchased");
    }

    public void OnPurchaseFailure(Product product, PurchaseFailureReason reason)
    {
        Debug.Log("Purchase of product " + product.definition.id + " failed because " + reason);
    }


    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}