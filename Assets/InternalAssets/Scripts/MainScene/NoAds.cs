﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

public class NoAds : Buttons, IEventSub, IAdsStateChecker
{
    private int _isNoAdsPurchased;
    private bool _isInternetConnectionAccess;

    void Start()
    {
        StartCoroutine(CheckInternetConnection());
    }

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
    }

    public void CheckNoAdsPurchaseState()
    {
        if (PlayerPrefs.HasKey("FlyArr_NoAds"))
            _isNoAdsPurchased = PlayerPrefs.GetInt("FlyArr_NoAds");
        if (_isNoAdsPurchased == 1)
        {
            gameObject.SetActive(false);
        }
    }

    private IEnumerator CheckInternetConnection()
    {
        CheckNoAdsPurchaseState();
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            _isInternetConnectionAccess = false;
        }
        else
        {
            _isInternetConnectionAccess = true;
        }

        CheckNoAdsPurchaseState();
        Subscribe();
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("NoAdsPurchased", CheckNoAdsPurchaseState);
    }

    public void UnSubscribe()
    {
        StartListening("NoAdsPurchased", CheckNoAdsPurchaseState);
    }
}