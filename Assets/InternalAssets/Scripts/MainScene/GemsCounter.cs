﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GemsCounter : MonoBehaviour
{
    private int _globalGemsNumber;
    public TextMeshProUGUI globalGemsNumberTxt;

    void Start()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        globalGemsNumberTxt.text = _globalGemsNumber.ToString();
    }
}