﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip music;

    private static MusicPlayer _instance = null;

    // Start is called before the first frame update
    void Start()
    {
        if (_instance == null)
        {
            AudioPlayer.Instance.PlayMusic(music);
            _instance = this;
        }
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        //Set AudioPlayer to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
}