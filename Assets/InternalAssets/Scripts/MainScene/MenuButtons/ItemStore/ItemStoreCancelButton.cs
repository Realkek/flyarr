﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemStoreCancelButton : Buttons, ITick
{
    [SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject itemStore;
    [SerializeField] private GameObject mainArrow;


    private void Start()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && itemStore.activeSelf)
        {
            Cancel();
        }
    }

    public void Cancel()
    {
        mainScreen.SetActive(true);
        mainArrow.SetActive(true);
        itemStore.SetActive(false);
    }
}