﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombsButton : Buttons
{
    [SerializeField] private GameObject itemStore;
    [SerializeField] private GameObject bombs;
    [SerializeField] private CurrentSceneState currentSceneState;

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
        bombs.SetActive(true);
        itemStore.SetActive(false);
        currentSceneState.canvasInvolvedNow = "BombsCanvas";
    }
}