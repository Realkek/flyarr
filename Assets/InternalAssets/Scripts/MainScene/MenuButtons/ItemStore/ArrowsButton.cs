﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowsButton : Buttons
{
    [SerializeField] private GameObject itemStore;
    [SerializeField] private GameObject arrows;
    [SerializeField] private CurrentSceneState currentSceneState;

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
        arrows.SetActive(true);
        itemStore.SetActive(false);
        currentSceneState.canvasInvolvedNow = "ArrowsCanvas";
    }
}