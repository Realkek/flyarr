﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowsBackButton : MonoBehaviour
{
    [SerializeField] private GameObject itemStore;
    [SerializeField] private GameObject arrows;
    [SerializeField] private GameObject purchaseConfirmation;
    [SerializeField] private GameObject purchaseRestriction;

    private void OnMouseDown()
    {
        itemStore.SetActive(true);
        arrows.SetActive(false);
        purchaseConfirmation.SetActive(false);
        purchaseRestriction.SetActive(false);
    }
}