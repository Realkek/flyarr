﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowsCancelButton : MonoBehaviour, ITick
{
    [SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject arrows;
    [SerializeField] private GameObject mainArrow;
    [SerializeField] private GameObject purchaseConfirmation;
    [SerializeField] private GameObject purchaseRestriction;

    private void Start()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && arrows.activeSelf)
        {
            Cancel();
        }
    }

    private void OnMouseDown()
    {
        Cancel();
    }

    private void Cancel()
    {
        arrows.SetActive(false);
        mainScreen.SetActive(true);
        mainArrow.SetActive(true);
        purchaseConfirmation.SetActive(false);
        purchaseRestriction.SetActive(false);
    }
}