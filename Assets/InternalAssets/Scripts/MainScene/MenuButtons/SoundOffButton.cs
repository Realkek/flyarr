﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOffButton : MonoBehaviour, IEventTrigger
{
    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    private void OnMouseUp()
    {
        TriggerEvent("soundOffButtonClicked");
    }
}