﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicOnButton : MonoBehaviour, IEventTrigger
{
    [SerializeField] private EventsCollection musicOnButtonClicked;


    private void OnMouseUp()
    {
        TriggerEvent(musicOnButtonClicked.currentEvent);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
}