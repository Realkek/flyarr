﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundButton : Buttons
{
    [SerializeField] private GameObject themesCanvas;
    [SerializeField] private GameObject backgroundPanel;
    [SerializeField] private CurrentSceneState currentSceneState;

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
        themesCanvas.SetActive(true);
        backgroundPanel.SetActive(true);
        currentSceneState.canvasInvolvedNow = "ThemesCanvas";
    }
}