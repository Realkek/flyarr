﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemesBackButton : MonoBehaviour, ITick
{
    [SerializeField] private GameObject themesCanvas;

    private void Start()
    {
        ManagerUpdate.AddTo(this);
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            themesCanvas.SetActive(false);
        }
    }
}