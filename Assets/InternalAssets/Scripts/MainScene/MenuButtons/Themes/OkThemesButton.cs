﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OkThemesButton : MonoBehaviour
{
    [SerializeField] private GameObject themesCanvas;

    public void HideThemesCanvas()
    {
        themesCanvas.SetActive(false);
    }
}