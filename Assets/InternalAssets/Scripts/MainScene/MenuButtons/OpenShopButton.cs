﻿using System;
using UnityEngine;

public class OpenShopButton : Buttons, IEventTrigger
{
    [SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject itemStore;
    [SerializeField] private GameObject mainArrow;
    [SerializeField] private GameObject settingsButtons;

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        itemStore.SetActive(true);
        mainScreen.SetActive(false);
        mainArrow.SetActive(false);
        TriggerEvent("ShopOpened");
        ResetButtonSize();
    }

    private void HideSettingsButtons()
    {
        for (int childNumber = 0; childNumber < settingsButtons.transform.childCount; childNumber++)
        {
            settingsButtons.transform.GetChild(childNumber).gameObject.SetActive(false);
        }
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}