﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SoundButton : MonoBehaviour, IEventSub
{
    private string _isSoundButtonState;

    // Start is called before the first frame update
    void Start()
    {
        Subscribe();
        if (PlayerPrefs.HasKey("isSoundButtonState"))
        {
            _isSoundButtonState = PlayerPrefs.GetString("isSoundButtonState");
            if (_isSoundButtonState == "ON")
            {
                UnmuteSound();
            }
            else
            {
                MuteSound();
            }
        }
        else
        {
            UnmuteSound();
        }
    }

    void MuteSound()
    {
        AudioPlayer.Instance.EffectsSource.mute = true;
        PlayerPrefs.SetString("isSoundButtonState", "OFF");
    }

    void UnmuteSound()
    {
        AudioPlayer.Instance.EffectsSource.mute = false;
        PlayerPrefs.SetString("isSoundButtonState", "ON");
    }

    void ShowSoundOnButton()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(false);
        UnmuteSound();
    }

    void ShowSoundOffButton()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
        MuteSound();
    }

    void ShowSoundButtons()
    {
        if (PlayerPrefs.HasKey("isSoundButtonState"))
        {
            _isSoundButtonState = PlayerPrefs.GetString("isSoundButtonState");
            if (_isSoundButtonState == "ON")
            {
                ShowSoundOnButton();
            }
            else
            {
                ShowSoundOffButton();
            }
        }
        else
        {
            ShowSoundOnButton();
        }
    }

    void HideSoundButtons()
    {
        for (int childNumber = 0; childNumber < transform.childCount; childNumber++)
        {
            transform.GetChild(childNumber).gameObject.SetActive(false);
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("soundOnButtonClicked", ShowSoundOffButton);
        StartListening("soundOffButtonClicked", ShowSoundOnButton);
        StartListening("SettingsButtonActivated", ShowSoundButtons);
        StartListening("SettingsButtonDeActivated", HideSoundButtons);
    }

    public void UnSubscribe()
    {
        StopListening("soundOnButtonClicked", ShowSoundOffButton);
        StopListening("soundOffButtonClicked", ShowSoundOnButton);
        StopListening("settingsButtonActivated", ShowSoundButtons);
        StopListening("settingsButtonDeActivated", HideSoundButtons);
    }
}