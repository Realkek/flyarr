﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppExitButton : MonoBehaviour, ITick
{
    [SerializeField] private GameObject mainScreenCanvas;
    [SerializeField] private GameObject themesCanvas;

    private void Start()
    {
        ManagerUpdate.AddTo(this);
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("isAppFirstStart", 0);
    }

    public void Tick()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && mainScreenCanvas.activeSelf && themesCanvas.activeSelf == false)
        {
            PlayerPrefs.SetInt("isAppFirstStart", 0);
            Application.Quit();
        }
    }
}