﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CurrentArrowContent : MonoBehaviour, IEventSub
{
    [SerializeField] public GameObject purchaseConfirmationCanvas;
    [SerializeField] public GameObject purchaseRestrictionCanvas;
    [SerializeField] public GameObject mainSceneArrowObject;
    [SerializeField] private GameObject buyButton;
    [SerializeField] private GameObject selectButton;
    [SerializeField] public ArrowData chosenArrow;
    [SerializeField] private GameObject chosenArrowImage;
    public ArrowData currentArrowData;
    private string _chosenArrowState;
    public string isPurchased;


    private void Start()
    {
        Subscribe();
        ChangeButtonsByPurchasedState();
    }

    private void ChangeButtonsByPurchasedState()
    {
        _chosenArrowState = PlayerPrefs.GetString($"{currentArrowData.ItemName}");
        isPurchased = PlayerPrefs.GetString($"{currentArrowData.Id}");
        if (_chosenArrowState == "isChosen")
        {
            buyButton.SetActive(false);
            selectButton.SetActive(false);
            chosenArrowImage.SetActive(true);
        }
        else if (isPurchased == "isPurchased")
        {
            buyButton.SetActive(false);
            chosenArrowImage.SetActive(false);
            selectButton.SetActive(true);
        }
        else
        {
            chosenArrowImage.SetActive(false);
            selectButton.SetActive(false);
            buyButton.SetActive(true);
        }
    }


    private void ForgettingChosenPreviousArrows()
    {
        PlayerPrefs.SetString($"{currentArrowData.ItemName}", "isNotChosen");
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("arrowPurchased", ChangeButtonsByPurchasedState);
        StartListening("chosenNewArrow", ForgettingChosenPreviousArrows);
        StartListening("arrowSelected", ChangeButtonsByPurchasedState);
    }

    public void UnSubscribe()
    {
        StopListening("arrowPurchased", ChangeButtonsByPurchasedState);
        StopListening("chosenNewArrow", ForgettingChosenPreviousArrows);
        StopListening("arrowSelected", ChangeButtonsByPurchasedState);
    }
}