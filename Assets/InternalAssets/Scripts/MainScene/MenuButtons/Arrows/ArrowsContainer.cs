﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ArrowsContainer", menuName = "ArrowsContainer", order = 51)]
public class ArrowsContainer : ScriptableObject
{
    [SerializeField] private List<ArrowData> arrowsDataContainer;

    public List<ArrowData> ArrowsDataContainer
    {
        get => arrowsDataContainer;
        set => arrowsDataContainer = value;
    }
}