﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class BuyArrowButton : MonoBehaviour, IEventTrigger, IEventSub
{
    private GameObject _purchaseConfirmationCanvas;
    private GameObject _purchaseRestrictionCanvas;
    private GameObject _mainSceneArrowObject;
    private int _globalGemsNumber;
    public TextMeshProUGUI arrowsGemsNumberTxt;
    private ArrowData _currentArrowData;
    private ArrowData _chosenArrow;
    private TextMeshProUGUI _textMeshProUgui;

    private void Start()
    {
        Subscribe();
        _purchaseRestrictionCanvas = transform.GetComponentInParent<CurrentArrowContent>().purchaseRestrictionCanvas;
        _purchaseConfirmationCanvas = transform.GetComponentInParent<CurrentArrowContent>().purchaseConfirmationCanvas;
        _mainSceneArrowObject = transform.GetComponentInParent<CurrentArrowContent>().mainSceneArrowObject;
        _currentArrowData = transform.GetComponentInParent<CurrentArrowContent>().currentArrowData;
        _chosenArrow = transform.GetComponentInParent<CurrentArrowContent>().chosenArrow;
        transform.GetComponentInParent<TextMeshProUGUI>();
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
    }

    private void ReceiveGemsNumber()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        arrowsGemsNumberTxt.text = _globalGemsNumber.ToString();
    }

    public void SetCurrentBuyingArrowName()
    {
        _chosenArrow.ItemName = _currentArrowData.ItemName;
    }


    public void ChooseWhichToShowWindow()
    {
        if (_globalGemsNumber >= _currentArrowData.GoldCost)
        {
            ShowConfirmWindow();
        }
        else
        {
            ShowCancelWindow();
        }
    }

    private void ShowConfirmWindow()
    {
        _purchaseConfirmationCanvas.SetActive(true);
        _purchaseRestrictionCanvas.SetActive(false);
    }

    private void ShowCancelWindow()
    {
        _purchaseConfirmationCanvas.SetActive(false);
        _purchaseRestrictionCanvas.SetActive(true);
    }

    private void BuyCurrentArrow()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        if (_chosenArrow.ItemName == _currentArrowData.ItemName)
        {
            _globalGemsNumber = _globalGemsNumber - _currentArrowData.GoldCost;
            PlayerPrefs.SetInt("GlobalGemsNumber", _globalGemsNumber);
            arrowsGemsNumberTxt.text = _globalGemsNumber.ToString();
            SetPurchasedState();
            TriggerEvent("GemsNumberChanged");
            ChoosingArrowImage();
            TriggerEvent("chosenNewArrow");
            PlayerPrefs.SetString($"{_currentArrowData.ItemName}", "isChosen");
            TriggerEvent("arrowPurchased");
            _purchaseConfirmationCanvas.SetActive(false);
        }
    }


    private void ChoosingArrowImage()
    {
        PlayerPrefs.SetString("ChosenArrowId", _currentArrowData.Id);
        _mainSceneArrowObject.GetComponent<SpriteRenderer>().sprite =
            _currentArrowData.Icon;
    }

    private void SetPurchasedState()
    {
        PlayerPrefs.SetString($"{_currentArrowData.Id}", "isPurchased");
    }


    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }
    
    public void Subscribe()
    {
        StartListening("YesBuyArrowsCanvasButtonClicked", BuyCurrentArrow);
        StartListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void UnSubscribe()
    {
        StopListening("YesBuyArrowsCanvasButtonClicked", BuyCurrentArrow);
        StopListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}