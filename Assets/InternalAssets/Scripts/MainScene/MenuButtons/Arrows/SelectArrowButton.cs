﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectArrowButton : MonoBehaviour, IEventTrigger
{
    private ArrowData _currentArrowData;
    private GameObject _mainSceneArrowObject;
    [SerializeField] private AudioClip audioClip;

    private void Start()
    {
        _currentArrowData = transform.GetComponentInParent<CurrentArrowContent>().currentArrowData;
        _mainSceneArrowObject = transform.GetComponentInParent<CurrentArrowContent>().mainSceneArrowObject;
    }

    public void SelectArrow()
    {
        AudioPlayer.Instance.Play(audioClip);
        ChoosingArrowImage();
        TriggerEvent("chosenNewArrow");
        PlayerPrefs.SetString($"{_currentArrowData.ItemName}", "isChosen");
        PlayerPrefs.SetString("ChosenArrowId", _currentArrowData.Id);
        TriggerEvent("arrowSelected");
    }

    private void ChoosingArrowImage()
    {
        _mainSceneArrowObject.GetComponent<SpriteRenderer>().sprite =
            _currentArrowData.Icon;
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}