﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartArrowSelection : MonoBehaviour
{
    void Awake()
    {
        if (PlayerPrefs.HasKey("isAppFirstStart") == false)
        {
            PlayerPrefs.SetString("1_PurpleArrow1", "isPurchased");
            ManagerEvents.TriggerEvent("chosenNewArrow");
            PlayerPrefs.SetString("1_PurpleArrow", "isChosen");
            PlayerPrefs.SetString("ChosenArrowId", "1_PurpleArrow1");
            ManagerEvents.TriggerEvent("arrowPurchased");
        }
    }
}