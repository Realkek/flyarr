﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JewelsCanvasButton : Buttons
{
    [SerializeField] private GameObject jewelsCanvas;
    [SerializeField] private GameObject itemStore;

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
        jewelsCanvas.SetActive(true);
        itemStore.SetActive(false);
    }
}