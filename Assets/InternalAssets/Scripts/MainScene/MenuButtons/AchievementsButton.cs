﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class AchievementsButton : Buttons, IEventSub
{
    [SerializeField] private GameObject themesCanvas;
    [SerializeField] private GameObject mainScreen;
    [SerializeField] private GameObject mainArrow;

    private void Start()
    {
        if (Social.localUser.authenticated == false)
            GetComponent<Image>().color = new Color32(255, 255, 225, 100);
        Subscribe();
    }

    private void ShowAhievementsButton()
    {
        GetComponent<Image>().color = new Color32(255, 255, 225, 255);
    }

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        ResetButtonSize();
        if (Social.localUser.authenticated)
        {
            Social.ShowAchievementsUI();
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GPGSUserAuthenticated", ShowAhievementsButton);
    }

    public void UnSubscribe()
    {
        StopListening("GPGSUserAuthenticated", ShowAhievementsButton);
    }
}