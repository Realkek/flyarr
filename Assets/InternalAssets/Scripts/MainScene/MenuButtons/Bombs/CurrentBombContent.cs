﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CurrentBombContent : MonoBehaviour, IEventSub
{
    [SerializeField] public GameObject purchaseConfirmationCanvas;
    [SerializeField] public GameObject purchaseRestrictionCanvas;
    [SerializeField] private GameObject buyButton;
    [SerializeField] private GameObject selectButton;
    [SerializeField] public ArrowData chosenBomb;
    public ArrowData currentBombData;
    [SerializeField] private GameObject chosenBombImage;
    private string _chosenBombState;
    public string isPurchased;
    private int _isAppFirstStart = 1;

    private void Start()
    {
        Subscribe();
        ChangeButtonsByPurchasedState();
    }

    private void ChangeButtonsByPurchasedState()
    {
        _chosenBombState = PlayerPrefs.GetString($"{currentBombData.ItemName}");
        isPurchased = PlayerPrefs.GetString($"{currentBombData.Id}");

        if (_chosenBombState == "isChosen")
        {
            buyButton.SetActive(false);
            selectButton.SetActive(false);
            chosenBombImage.SetActive(true);
        }
        else if (isPurchased == "isPurchased")
        {
            buyButton.SetActive(false);
            chosenBombImage.SetActive(false);
            selectButton.SetActive(true);
        }
        else
        {
            chosenBombImage.SetActive(false);
            selectButton.SetActive(false);
            buyButton.SetActive(true);
        }
    }

    private void ForgettingChosenPreviousBombs()
    {
        PlayerPrefs.SetString($"{currentBombData.ItemName}", "isNotChosen");
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("bombPurchased", ChangeButtonsByPurchasedState);
        StartListening("chosenNewBomb", ForgettingChosenPreviousBombs);
        StartListening("bombSelected", ChangeButtonsByPurchasedState);
    }

    public void UnSubscribe()
    {
        StopListening("bombPurchased", ChangeButtonsByPurchasedState);
        StopListening("chosenNewBomb", ForgettingChosenPreviousBombs);
        StopListening("bombSelected", ChangeButtonsByPurchasedState);
    }
}