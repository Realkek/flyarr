﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class BuyBombButton : MonoBehaviour, IEventSub, IEventTrigger
{
    private GameObject _purchaseConfirmationCanvas;
    private GameObject _purchaseRestrictionCanvas;
    private int _globalGemsNumber;
    public TextMeshProUGUI arrowsGemsNumberTxt;
    private ArrowData _currentBombData;
    private ArrowData _chosenBomb;
    private TextMeshProUGUI _textMeshProUgui;
    private string _itemShopName;

    private void Start()
    {
        Subscribe();
        _purchaseRestrictionCanvas = transform.GetComponentInParent<CurrentBombContent>().purchaseRestrictionCanvas;
        _purchaseConfirmationCanvas = transform.GetComponentInParent<CurrentBombContent>().purchaseConfirmationCanvas;
        _currentBombData = transform.GetComponentInParent<CurrentBombContent>().currentBombData;
        _chosenBomb = transform.GetComponentInParent<CurrentBombContent>().chosenBomb;
        transform.GetComponentInParent<TextMeshProUGUI>();
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
    }

    private void ReceiveGemsNumber()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        arrowsGemsNumberTxt.text = _globalGemsNumber.ToString();
    }

    public void SetCurrentBuyingBombName()
    {
        {
            _chosenBomb.ItemName = _currentBombData.ItemName;
        }
    }


    public void ChooseWhichToShowWindow()
    {
        if (_globalGemsNumber >= _currentBombData.GoldCost)
        {
            ShowConfirmWindow();
        }
        else
        {
            ShowCancelWindow();
        }
    }

    private void ShowConfirmWindow()
    {
        _purchaseConfirmationCanvas.SetActive(true);
        _purchaseRestrictionCanvas.SetActive(false);
    }

    private void ShowCancelWindow()
    {
        _purchaseConfirmationCanvas.SetActive(false);
        _purchaseRestrictionCanvas.SetActive(true);
    }

    private void BuyCurrentBomb()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        if (_chosenBomb.ItemName == _currentBombData.ItemName)
        {
            _globalGemsNumber = _globalGemsNumber - _currentBombData.GoldCost;
            PlayerPrefs.SetInt("GlobalGemsNumber", _globalGemsNumber);
            arrowsGemsNumberTxt.text = _globalGemsNumber.ToString();
            SetPurchasedState();
            TriggerEvent("GemsNumberChanged");
            ChoosingBombImage();
            TriggerEvent("chosenNewBomb");
            PlayerPrefs.SetString($"{_currentBombData.ItemName}", "isChosen");
            TriggerEvent("bombPurchased");
            _purchaseConfirmationCanvas.SetActive(false);
        }
    }

    private void ChoosingBombImage()
    {
        PlayerPrefs.SetString("ChosenBombId", _currentBombData.Id);
    }


    private void SetPurchasedState()
    {
        PlayerPrefs.SetString($"{_currentBombData.Id}", "isPurchased");
    }


    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("YesBuyBombsCanvasButtonClicked", BuyCurrentBomb);
        StartListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void UnSubscribe()
    {
        StopListening("YesBuyBombsCanvasButtonClicked", BuyCurrentBomb);
        StopListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}