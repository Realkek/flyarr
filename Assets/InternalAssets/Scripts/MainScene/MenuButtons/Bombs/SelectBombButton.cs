﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectBombButton : MonoBehaviour, IEventTrigger
{
    private ArrowData _currentBombData;
    private ArrowData _chosenBomb;
    [SerializeField] private AudioClip audioClip;

    private void Start()
    {
        _currentBombData = transform.GetComponentInParent<CurrentBombContent>().currentBombData;
        _chosenBomb = transform.GetComponentInParent<CurrentBombContent>().chosenBomb;
    }

    public void SelectBomb()
    {
        AudioPlayer.Instance.Play(audioClip);
        ChoosingBombImage();
        TriggerEvent("chosenNewBomb");
        PlayerPrefs.SetString($"{_currentBombData.ItemName}", "isChosen");
        TriggerEvent("bombSelected");
    }

    private void ChoosingBombImage()
    {
        PlayerPrefs.SetString("ChosenBombId", _currentBombData.Id);
    }


    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}