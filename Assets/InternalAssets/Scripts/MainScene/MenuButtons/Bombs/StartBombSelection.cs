﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBombSelection : MonoBehaviour
{
    void Awake()
    {
        if (PlayerPrefs.HasKey("isAppFirstStart") == false)
        {
            PlayerPrefs.SetString("bombdef1", "isPurchased");
            ManagerEvents.TriggerEvent("chosenNewBomb");
            PlayerPrefs.SetString("bombdef", "isChosen");
            ManagerEvents.TriggerEvent("bombPurchased");
        }
    }
}