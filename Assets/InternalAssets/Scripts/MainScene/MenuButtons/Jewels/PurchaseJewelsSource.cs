﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class PurchaseJewelsSource : MonoBehaviour, IEventSub, IEventTrigger
{
    [SerializeField] private TextMeshProUGUI GemsNumberTxt;
    private int _globalGemsNumber;

    private void Start()
    {
        Subscribe();
    }

    public void OnPurchaseComplete(Product product)
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        if (product.definition.id == "jewels_100") _globalGemsNumber += 100;
        else if (product.definition.id == "jewels_250") _globalGemsNumber += 250;
        else if (product.definition.id == "jewels_500") _globalGemsNumber += 500;
        else if (product.definition.id == "jewels_1000") _globalGemsNumber += 1000;
        PlayerPrefs.SetInt("GlobalGemsNumber", _globalGemsNumber);
        TriggerEvent("GemsNumberChanged");
    }

    public void OnPurchaseFailure(Product product, PurchaseFailureReason reason)
    {
        Debug.Log("Purchase of product " + product.definition.id + " failed because " + reason);
    }

    private void ReceiveGemsNumber()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        GemsNumberTxt.text = _globalGemsNumber.ToString();
    }


    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void UnSubscribe()
    {
        StopListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}