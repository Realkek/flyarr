﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicOffButton : MonoBehaviour, IEventTrigger
{
    [SerializeField]
    private EventsCollection musicOffButtonClicked;

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    private void OnMouseUp()
    {
        TriggerEvent(musicOffButtonClicked.currentEvent);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
