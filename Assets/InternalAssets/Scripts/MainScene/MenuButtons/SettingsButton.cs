﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SettingsButton : Buttons, IEventSub, IEventTrigger
{
    [SerializeField] private EventsCollection Tap;
    [SerializeField] private EventsCollection settingsButtonActivated;
    [SerializeField] private EventsCollection settingsButtonDeActivated;
    private bool _isSettingButtonActivated;

    void Start()
    {
        Subscribe();
    }

    private void OnMouseDown()
    {
        IncreaseButtonSize();
    }

    private void OnMouseUp()
    {
        SelectViewStatus(false);
        ResetButtonSize();
    }

    private void ButtonsReset()
    {
        TriggerEvent(settingsButtonDeActivated.currentEvent);
        _isSettingButtonActivated = false;
    }

    void SelectViewStatus(bool isTap)
    {
        for (int childNumber = 0; childNumber < transform.childCount; childNumber++)
        {
            GameObject child = transform.GetChild(childNumber).gameObject;
            if (_isSettingButtonActivated == false && isTap != true)
            {
                TriggerEvent(settingsButtonActivated.currentEvent);
                _isSettingButtonActivated = true;
            }
            else if (_isSettingButtonActivated && isTap != true)
            {
                TriggerEvent(settingsButtonDeActivated.currentEvent);
                _isSettingButtonActivated = false;
            }
            else
            {
                child.SetActive(false);
            }
        }
    }

    void OnTapViewSelectStatus()
    {
        SelectViewStatus(true);
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(Tap.currentEvent, OnTapViewSelectStatus);
        StartListening("ShopOpened", ButtonsReset);
    }

    public void UnSubscribe()
    {
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}