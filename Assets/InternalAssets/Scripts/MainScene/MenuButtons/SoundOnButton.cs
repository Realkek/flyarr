﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnButton : MonoBehaviour, IEventTrigger
{
    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    private void OnMouseUp()
    {
        TriggerEvent("soundOnButtonClicked");
    }
}