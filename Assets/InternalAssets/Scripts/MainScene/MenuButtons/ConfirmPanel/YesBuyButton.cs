﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class YesBuyButton : MonoBehaviour, IEventTrigger
{
    [SerializeField] private CurrentSceneState currentSceneState;
    [SerializeField] private AudioClip audioClip;

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    public void TriggerEventByNameCanvas()
    {
        AudioPlayer.Instance.Play(audioClip);
        TriggerEvent($"YesBuy{currentSceneState.canvasInvolvedNow}ButtonClicked");
    }
}