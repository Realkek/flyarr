﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoBuyButton : MonoBehaviour
{
    [SerializeField] private GameObject noBuyButton;

    public void HideConfirmationCanvas()
    {
        noBuyButton.SetActive(false);
    }
}