﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour
{
    [SerializeField] private AudioClip audioClip;


    protected void IncreaseButtonSize()
    {
        transform.localScale = new Vector3(transform.localScale.x * 1.1f, transform.localScale.y * 1.1f
        );
        AudioPlayer.Instance.Play(audioClip);
    }

    protected void ResetButtonSize()
    {
        transform.localScale = new Vector3(transform.localScale.x / 1.1f, transform.localScale.y / 1.1f
        );
    }
}