﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ArrowReward : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject present;
    [SerializeField] private List<ArrowData> arrowDataList;
    private int _randomNumber;
    private string _lastRewardedTime;
    private TimeSpan _timeSpan;
    [SerializeField] private GameObject arrowCase;
    private string _isPurchased;
    private int _isAppFirstStart = 1;

    private void Awake()
    {
        ShowRandomChosenArrow();
        ReceivePurchasedState();
        CheckRewardedState();
        Subscribe();
    }


    private void ShowRandomChosenArrow()
    {
        _randomNumber = Random.Range(0, arrowDataList.Count);
        arrowCase.GetComponent<Image>().sprite = arrowDataList[_randomNumber].Icon;
    }

    private void ReceiveChosenArrow()
    {
        PlayerPrefs.SetString($"{arrowDataList[_randomNumber].Id}", "isPurchased");
    }

    private void ReceivePurchasedState()
    {
        _isPurchased = PlayerPrefs.GetString($"{arrowDataList[_randomNumber].Id}");
    }

    private void CheckRewardedState()
    {
        if (PlayerPrefs.HasKey("isAppFirstStart"))
            _isAppFirstStart = 0;
        else
        {
            present.SetActive(true);
        }

        if (PlayerPrefs.HasKey("ArrowRewardedTime"))
        {
            _lastRewardedTime = PlayerPrefs.GetString("ArrowRewardedTime");
            _timeSpan = DateTime.Now - DateTime.Parse(_lastRewardedTime);
        }

        if (_isAppFirstStart == 0)
        {
            if (_timeSpan.Hours <= 2 && _timeSpan.Milliseconds != 0 || _isPurchased == "isPurchased")
                gameObject.SetActive(false);
            else
            {
                present.SetActive(true);
            }
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("ArrowReceived", ReceiveChosenArrow);
        StartListening("ArrowReceived", CheckRewardedState);
    }

    public void UnSubscribe()
    {
        StopListening("ArrowReceived", ReceiveChosenArrow);
        StopListening("ArrowReceived", CheckRewardedState);
    }
}