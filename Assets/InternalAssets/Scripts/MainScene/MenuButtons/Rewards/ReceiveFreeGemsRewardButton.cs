﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ReceiveFreeGemsRewardButton : MonoBehaviour, IEventTrigger, IEventSub
{
    [SerializeField] private Text gemsCountText;
    private int _gemsCount;
    [SerializeField] private TextMeshProUGUI globalGemsCount;
    [SerializeField] private GameObject rewardGemsCase;
    private int _globalGemsCount;
    [SerializeField] private AudioClip audioClip;

    private void Start()
    {
        Subscribe();
    }

    private void ReceiveGemsNumber()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsCount = PlayerPrefs.GetInt("GlobalGemsNumber");
        globalGemsCount.text = _globalGemsCount.ToString();
    }

    public void ReceiveGems()
    {
        PlayerPrefs.SetString("freeGemsRewardedTime", DateTime.Now.ToString(CultureInfo.InvariantCulture));
        _gemsCount = Convert.ToInt32(gemsCountText.text);
        _globalGemsCount = Convert.ToInt32(globalGemsCount.text);
        _globalGemsCount = _globalGemsCount + _gemsCount;
        PlayerPrefs.SetInt("GlobalGemsNumber", _globalGemsCount);
        globalGemsCount.text = _globalGemsCount.ToString();
        TriggerEvent("GemsNumberChanged");
        TriggerEvent("FreeGemsReceived");
        TriggerEvent("DailyRewardReceived");
        AudioPlayer.Instance.Play(audioClip);
        rewardGemsCase.SetActive(false);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void UnSubscribe()
    {
        StopListening("GemsNumberChanged", ReceiveGemsNumber);
    }
}