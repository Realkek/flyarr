﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using GoogleMobileAds.Api;
using UnityEngine;

public class ReceiveBombReward : MonoBehaviour, IEventTrigger
{
    [SerializeField] private GameObject rewardBombCase;
    [SerializeField] private AudioClip audioClip;
    private RewardedAd rewardedAd;
    private const string adUnitId = "ca-app-pub-1542106522436399/4881403469";

    private void Start()
    {
        this.rewardedAd = new RewardedAd(adUnitId);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);

        // Called when an ad request has successfully loaded.
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;
    }

    private void ReceiveBomb()
    {
        AudioPlayer.Instance.Play(audioClip);
        PlayerPrefs.SetString("BombRewardedTime", DateTime.Now.ToString(CultureInfo.InvariantCulture));
        TriggerEvent("BombReceived");
        TriggerEvent("bombPurchased");
        TriggerEvent("DailyRewardReceived");
        rewardBombCase.SetActive(false);
    }

    public void ShowAds()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
        }
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        rewardBombCase.SetActive(false);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        rewardBombCase.SetActive(false);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        rewardBombCase.SetActive(false);
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        ReceiveBomb();
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}