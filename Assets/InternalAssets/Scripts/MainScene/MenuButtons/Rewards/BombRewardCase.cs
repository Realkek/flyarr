﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BombRewardCase : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject present;
    [SerializeField] private List<ArrowData> bombDataList;
    private int _randomNumber;
    private string _lastRewardedTime;
    private TimeSpan _timeSpan;
    [SerializeField] private GameObject bombCase;
    private string _isPurchased;
    private int _isAppFirstStart = 1;

    private void Awake()
    {
        ShowRandomChosenBomb();
        ReceivePurchasedState();
        CheckRewardedState();
        Subscribe();
    }


    private void ShowRandomChosenBomb()
    {
        _randomNumber = Random.Range(0, bombDataList.Count);
        bombCase.GetComponent<Image>().sprite = bombDataList[_randomNumber].Icon;
    }

    private void ReceiveChosenBomb()
    {
        PlayerPrefs.SetString($"{bombDataList[_randomNumber].Id}", "isPurchased");
    }

    private void ReceivePurchasedState()
    {
        _isPurchased = PlayerPrefs.GetString($"{bombDataList[_randomNumber].Id}");
    }

    private void CheckRewardedState()
    {
        if (PlayerPrefs.HasKey("isAppFirstStart"))
            _isAppFirstStart = 0;
        else
        {
            present.SetActive(true);
        }

        if (PlayerPrefs.HasKey("BombRewardedTime"))
        {
            _lastRewardedTime = PlayerPrefs.GetString("BombRewardedTime");
            _timeSpan = DateTime.Now - DateTime.Parse(_lastRewardedTime);
        }

        if (_isAppFirstStart == 0)
        {
            if (_timeSpan.Hours <= 2 && _timeSpan.Milliseconds != 0 || _isPurchased == "isPurchased")
                gameObject.SetActive(false);
            else
            {
                present.SetActive(true);
            }
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("BombReceived", ReceiveChosenBomb);
        StartListening("BombReceived", CheckRewardedState);
    }

    public void UnSubscribe()
    {
        StopListening("BombReceived", ReceiveChosenBomb);
        StopListening("BombReceived", CheckRewardedState);
    }
}