﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FreeGemsRewardCase : MonoBehaviour, IEventSub
{
    [SerializeField] private GameObject present;
    private string _lastRewardedTime;
    private TimeSpan _timeSpan;
    private int _isAppFirstStart = 1;

    private void Awake()
    {
        Subscribe();
        CheckRewardedState();
    }

    private void CheckRewardedState()
    {
        if (PlayerPrefs.HasKey("isAppFirstStart"))
            _isAppFirstStart = 0;
        else
        {
            present.SetActive(true);
        }

        if (PlayerPrefs.HasKey("freeGemsRewardedTime"))
        {
            try
            {
                _lastRewardedTime = PlayerPrefs.GetString("freeGemsRewardedTime");
                _timeSpan = DateTime.Now - DateTime.Parse(_lastRewardedTime);
            }
            catch
            {
                gameObject.SetActive(false);
            }
        }

        if (_isAppFirstStart == 0)
        {
            if (_timeSpan.Hours <= 2 && _timeSpan.Milliseconds != 0)
                gameObject.SetActive(false);
            else
            {
                present.SetActive(true);
            }
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("FreeGemsReceived", CheckRewardedState);
    }

    public void UnSubscribe()
    {
        StopListening("FreeGemsReceived", CheckRewardedState);
    }
}