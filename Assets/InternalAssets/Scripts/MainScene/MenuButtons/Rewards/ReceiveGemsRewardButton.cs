﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using GoogleMobileAds.Api;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ReceiveGemsRewardButton : MonoBehaviour, IEventTrigger, IEventSub
{
    [SerializeField] private Text gemsCountText;
    private int _gemsCount;
    [SerializeField] private TextMeshProUGUI globalGemsCount;
    [SerializeField] private GameObject rewardGemsCase;
    private int _globalGemsCount;
    [SerializeField] private AudioClip audioClip;
    private RewardedAd rewardedAd;
    private const string adUnitId = "ca-app-pub-1542106522436399/5406240971";

    private void Start()
    {
        Subscribe();
        this.rewardedAd = new RewardedAd(adUnitId);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);

        // Called when an ad request has successfully loaded.
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;
    }

    private void ReceiveGemsNumber()
    {
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsCount = PlayerPrefs.GetInt("GlobalGemsNumber");
        globalGemsCount.text = _globalGemsCount.ToString();
    }

    private void ReceiveGems()
    {
        AudioPlayer.Instance.Play(audioClip);
        _gemsCount = Convert.ToInt32(gemsCountText.text);
        _globalGemsCount = Convert.ToInt32(globalGemsCount.text);
        _globalGemsCount = _globalGemsCount + _gemsCount;
        PlayerPrefs.SetInt("GlobalGemsNumber", _globalGemsCount);
        globalGemsCount.text = _globalGemsCount.ToString();
        PlayerPrefs.SetString("GemsRewardedTime", DateTime.Now.ToString(CultureInfo.InvariantCulture));
        TriggerEvent("GemsReceived");
        TriggerEvent("DailyRewardReceived");
        rewardGemsCase.SetActive(false);
    }

    public void ShowAds()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
        }
    }


    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        rewardGemsCase.SetActive(false);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        rewardGemsCase.SetActive(false);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        rewardGemsCase.SetActive(false);
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        ReceiveGems();
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GemsNumberChanged", ReceiveGemsNumber);
    }

    public void UnSubscribe()
    {
        StopListening("GemsNumberChanged", ReceiveGemsNumber);
    }
}