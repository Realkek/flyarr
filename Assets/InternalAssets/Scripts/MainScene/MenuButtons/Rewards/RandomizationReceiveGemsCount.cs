﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RandomizationReceiveGemsCount : MonoBehaviour
{
    [SerializeField] private Text gemsCountText;

    private void Start()
    {
        int randomGemsCount = Random.Range(5, 15);
        gemsCountText.text = randomGemsCount.ToString();
    }
}
