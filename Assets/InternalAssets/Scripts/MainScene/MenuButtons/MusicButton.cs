﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MusicButton : MonoBehaviour, IEventSub
{
    [SerializeField] private EventsCollection musicOffButtonClicked;
    [SerializeField] private EventsCollection musicOnButtonClicked;
    [SerializeField] private EventsCollection settingsButtonActivated;
    [SerializeField] private EventsCollection settingsButtonDeActivated;
    private string _isMusicButtonState;

    private void Start()
    {
        Subscribe();
        if (PlayerPrefs.HasKey("isMusicButtonState"))
        {
            _isMusicButtonState = PlayerPrefs.GetString("isMusicButtonState");
            if (_isMusicButtonState == "ON")
            {
                UnmuteMusic();
            }
            else
            {
                MuteMusic();
            }
        }
        else
        {
            UnmuteMusic();
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(musicOnButtonClicked.currentEvent, ShowMusicOffButton);
        StartListening(musicOffButtonClicked.currentEvent, ShowMusicOnButton);
        StartListening(settingsButtonActivated.currentEvent, ShowMusicButtons);
        StartListening(settingsButtonDeActivated.currentEvent, HideMusicButtons);
    }

    public void UnSubscribe()
    {
       
    }

    void MuteMusic()
    {
        AudioPlayer.Instance.MusicSource.mute = true;
        PlayerPrefs.SetString("isMusicButtonState", "OFF");
    }

    void UnmuteMusic()
    {
        AudioPlayer.Instance.MusicSource.mute = false;
        PlayerPrefs.SetString("isMusicButtonState", "ON");
    }

    void ShowMusicButtons()
    {
        if (PlayerPrefs.HasKey("isMusicButtonState"))
        {
            _isMusicButtonState = PlayerPrefs.GetString("isMusicButtonState");
            if (_isMusicButtonState == "ON")
            {
                ShowMusicOnButton();
            }
            else
            {
                ShowMusicOffButton();
            }
        }
        else
        {
            ShowMusicOnButton();
        }
    }

    void HideMusicButtons()
    {
        for (int childNumber = 0; childNumber < transform.childCount; childNumber++)
        {
            transform.GetChild(childNumber).gameObject.SetActive(false);
        }
    }

    void ShowMusicOnButton()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(false);
        UnmuteMusic();
    }

    void ShowMusicOffButton()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
        MuteMusic();
    }
}