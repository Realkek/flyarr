﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CurrentSceneState", menuName = "CurrentSceneState", order = 51)]
public class CurrentSceneState : ScriptableObject
{
    public string canvasInvolvedNow;
}