﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Events;

public class AdsBannerPanel : MonoBehaviour, IEventSub, IAdsStateChecker
{
    public BannerView bannerView;

    string adUnitId = "ca-app-pub-1542106522436399/8684917100";
    // string adUnitId = "ca-app-pub-3940256099942544/6300978111"; //test
    private int _isNoAdsPurchased = 0;

    private void Start()
    {
        CheckNoAdsPurchaseState();
        Subscribe();
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(initStatus => { });

        if (_isNoAdsPurchased == 0)
            RequestBanner();
    }

    public void CheckNoAdsPurchaseState()
    {
        if (PlayerPrefs.HasKey("flyarr_noads"))
            _isNoAdsPurchased = PlayerPrefs.GetInt("flyarr_noads");
    }

    private void RequestBanner()
    {
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        this.bannerView.OnAdOpening += this.HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        this.bannerView.OnAdClosed += this.HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        this.bannerView.LoadAd(request);
        bannerView.Show();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("NoAdsPurchased", CheckNoAdsPurchaseState);
    }

    public void UnSubscribe()
    {
        StartListening("NoAdsPurchased", CheckNoAdsPurchaseState);
    }
}