﻿using UnityEngine;
using UnityEngine.Events;

public class TapToPlay : MonoBehaviour, IEventTrigger, IEventSub
{
    [SerializeField] private EventsCollection Tap;
    [SerializeField] private GameObject ArrowView;
    [SerializeField] private GameObject ButtonsView;
    [SerializeField] private GameObject GameNameView;
    [SerializeField] private GameObject PlayTxtView;
    [SerializeField] private GameObject topPanel;
    [SerializeField] private GameObject botPanel;
    [SerializeField] private GameObject noAds;
    [SerializeField] private GameObject present;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private GameObject AdsBannerPanel;

    private void OnEnable()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        StartListening(Tap.currentEvent, StartSceneTransition);
    }

    public void UnSubscribe()
    {
        StopListening(Tap.currentEvent, StartSceneTransition);
    }

    private void StartSceneTransition()
    {
        AdsBannerPanel.GetComponent<AdsBannerPanel>().bannerView.Hide();
        UnSubscribe();
        AudioPlayer.Instance.Play(audioClip);
        PlayerPrefs.SetInt("isAppFirstStart", 0);
        HideObjAnim();
    }

    void HideObjAnim()
    {
        HideArrow();
        HideButtons();
        HideGameName();
        HidePlayTxtName();
        HideBotPanel();
        HideTopPanel();
        HideNoAds();
        HidePresent();
    }

    private void HidePresent()
    {
        present.GetComponent<Animation>().Play("HidePresent");
    }

    private void HideNoAds()
    {
        noAds.GetComponent<Animation>().Play("HideNoAds");
    }

    void HideArrow()
    {
        ArrowView.GetComponent<Animation>().Play();
    }

    void HideButtons()
    {
        Animation animButtons = ButtonsView.GetComponent<Animation>();
        animButtons.Play("HideButtons");
    }

    void HideGameName()
    {
        Animation animGameName = GameNameView.GetComponent<Animation>();
        animGameName.Play("HideGameName");
    }

    void HidePlayTxtName()
    {
        Animation animPlayTxtName = PlayTxtView.GetComponent<Animation>();
        animPlayTxtName.Play("HidePlayTxt");
    }

    void HideTopPanel()
    {
        Animation animPlayTopPanel = topPanel.GetComponent<Animation>();
        animPlayTopPanel.Play("HideTopPanel");
    }

    void HideBotPanel()
    {
        Animation animPlayBotPanel = botPanel.GetComponent<Animation>();
        animPlayBotPanel.Play("HideBotPannel");
    }

    private void OnMouseDown()
    {
        TriggerEvent(Tap.currentEvent);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener); //передаем название события и метод-обработчик
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener); //передаем название события и метод-обработчик
    }
}