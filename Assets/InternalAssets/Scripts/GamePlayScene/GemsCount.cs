﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GemsCount : MonoBehaviour, IEventSub
{
    [SerializeField] private EventsCollection gemsCollided;
    [SerializeField] EventsCollection gameOver;
    private int _globalGemsNumber;
    private Text _gemsCounterTxt;
    private int _gemsCounter;
    private int _gemsNumber = 0;

    [SerializeField] AudioClip audioClip;

    private void Start()
    {
        _gemsCounterTxt = GetComponent<Text>();
        if (PlayerPrefs.HasKey("GlobalGemsNumber"))
            _globalGemsNumber = PlayerPrefs.GetInt("GlobalGemsNumber");
        _gemsCounterTxt.text = _globalGemsNumber.ToString();
        Subscribe();
    }

    private void IncrementGemsCount()
    {
        AudioPlayer.Instance.Play(audioClip);
        _globalGemsNumber++;
        _gemsCounterTxt.text = _globalGemsNumber.ToString();
    }

    void AddGemsToGlobalGemsNumber()
    {
        PlayerPrefs.SetInt("GlobalGemsNumber", _globalGemsNumber);
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(gemsCollided.currentEvent, IncrementGemsCount);
        StartListening(gameOver.currentEvent, AddGemsToGlobalGemsNumber);
    }

    public void UnSubscribe()
    {
        StopListening(gemsCollided.currentEvent, IncrementGemsCount);
    }
}