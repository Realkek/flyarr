﻿using System.Collections;

public interface IGamePlayDataAwaiter
{
    IEnumerator Wait();
}