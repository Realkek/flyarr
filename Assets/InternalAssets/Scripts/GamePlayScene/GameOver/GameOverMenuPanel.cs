﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameOverMenuPanel : MonoBehaviour, IEventSub
{
    [SerializeField] private EventsCollection arrowCollided;
    [SerializeField] private EventsCollection secondChanceEarned;
    private bool _secondChanceRewardAdsIsLoaded;
    private int _childCount;
    Animation _anim;

    private bool _isSecondChanceEarned;

    private void Start()
    {
        _anim = GetComponent<Animation>();
        _childCount = transform.childCount;
        Subscribe();
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    private void ShowPanels()
    {
        if (_isSecondChanceEarned == false && _secondChanceRewardAdsIsLoaded)
            _anim.Play("ShowMenuPanel");
        else
            _anim.Play("ShowRetryButton");
    }

    private void HidePanels()
    {
        _isSecondChanceEarned = true;
        _anim.Play("HideMenuBorder");
        _anim.Stop("ShowMenuPanel");
        for (int i = 0; i < _childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void SetSecondChanceRewardAdsIsLoaded()
    {
        _secondChanceRewardAdsIsLoaded = true;
        ShowPanels();
    }

    public void Subscribe()
    {
        StartListening("SecondChanceRewardAdsIsLoaded", SetSecondChanceRewardAdsIsLoaded);
        StartListening("SecondChanceRewardAdsIsNotLoaded", ShowPanels);
        StartListening(secondChanceEarned.currentEvent, HidePanels);
    }

    public void UnSubscribe()
    {
    }
}