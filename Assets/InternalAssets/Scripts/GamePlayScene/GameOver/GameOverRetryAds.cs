﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Events;

public class GameOverRetryAds : MonoBehaviour, IEventSub, IAdsStateChecker
{
    private InterstitialAd interstitial;
    string adUnitId = "ca-app-pub-1542106522436399/4043513256";
    // string adUnitId = "ca-app-pub-3940256099942544/1033173712";// test
    private int _isNoAdsPurchased = 0;

    private void Start()
    {
        CheckNoAdsPurchaseState();
        Subscribe();
        RequestInterstitial();
    }

    private void RequestInterstitial()
    {
        this.interstitial = new InterstitialAd(adUnitId);
        // Called when an ad request has successfully loaded.
        this.interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        this.interstitial.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        this.interstitial.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        this.interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this.interstitial.LoadAd(request);
    }

    void ShowAds()
    {
        try
        {
            int gameOpenCounter = PlayerPrefs.GetInt("gameOpenCounter");
            if (this.interstitial.IsLoaded() && gameOpenCounter % 2 == 0 && _isNoAdsPurchased == 0)
            {
                this.interstitial.Show();
            }
        }
        
        catch
        {
            Debug.Log("LoadingAdsFailed");
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GameOver", ShowAds);
        StartListening("NoAdsPurchased", CheckNoAdsPurchaseState);
    }

    public void UnSubscribe()
    {
        StopListening("GameOver", ShowAds);
        StopListening("NoAdsPurchased", CheckNoAdsPurchaseState);
    }


    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    public void CheckNoAdsPurchaseState()
    {
        if (PlayerPrefs.HasKey("flyarr_noads"))
            _isNoAdsPurchased = PlayerPrefs.GetInt("flyarr_noads");
    }
}