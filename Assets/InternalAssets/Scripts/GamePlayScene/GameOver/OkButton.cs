﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OkButton : MonoBehaviour, IEventTrigger
{
    [SerializeField]
    EventsCollection secondChanceVideoClicked;
    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
    public void ClickOk()
    {
        TriggerEvent(secondChanceVideoClicked.currentEvent);
    }
}
