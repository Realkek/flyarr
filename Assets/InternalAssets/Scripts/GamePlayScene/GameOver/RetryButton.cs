﻿using System;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RetryButton : MonoBehaviour, IEventTrigger
{
    [SerializeField] EventsCollection gameOver;
    [SerializeField] private Text currentScore;
    [SerializeField] private AudioClip audioClip;
    private int _currentScore;
    private int _highScore;
    private const string LEADERBOARD = "CgkIkMnSxagHEAIQAg";
    private const string ACHIEVE1 = "CgkIkMnSxagHEAIQAw";
    private const string ACHIEVE2 = "CgkIkMnSxagHEAIQBA";
    private const string ACHIEVE3 = "CgkIkMnSxagHEAIQBg";
    private const string ACHIEVE4 = "CgkIkMnSxagHEAIQBQ";
    private const string ACHIEVE5 = "CgkIkMnSxagHEAIQBw";


    public void GoMainScene()
    {
        AudioPlayer.Instance.Play(audioClip);
        TriggerEvent(gameOver.currentEvent);
        try
        {
            RefreshScore();
            SendUserHighScore();
            ReceiveScoreAchieve();
        }
        catch
        {
            SceneManager.LoadScene("Main");
        }

        SceneManager.LoadScene("Main");
    }


    private void RefreshScore()
    {
        _currentScore = Convert.ToInt32(currentScore.text);
        PlayerPrefs.SetInt("currentScore", _currentScore);
        if (PlayerPrefs.HasKey("highScore"))
            _highScore = PlayerPrefs.GetInt("highScore");
        else
        {
            PlayerPrefs.SetInt("highScore", 0);
        }
    }

    private void SendUserHighScore()
    {
        if (_currentScore > _highScore)
        {
            PlayerPrefs.SetInt("highScore", _currentScore);
            Social.ReportScore(_currentScore, LEADERBOARD, (bool success) =>
            {
                if (success)
                    print("Success sent highscore");
                else
                {
                    print("Unsuccessfully sent highscore");
                }
            });
        }
    }


    private void ReceiveScoreAchieve()
    {
        if (_currentScore > 50)
            PlayGamesServicesInitializer.Instance.GetAchieve(ACHIEVE1);
        if (_currentScore > 100)
            PlayGamesServicesInitializer.Instance.GetAchieve(ACHIEVE2);
        if (_currentScore > 150)
            PlayGamesServicesInitializer.Instance.GetAchieve(ACHIEVE3);
        if (_currentScore > 200)
            PlayGamesServicesInitializer.Instance.GetAchieve(ACHIEVE4);
        if (_currentScore > 250)
            PlayGamesServicesInitializer.Instance.GetAchieve(ACHIEVE5);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}