﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private Text _timerText;

    private void Start()
    {
        _timerText = transform.GetChild(0).GetComponent<Text>();
    }

    public void Countdown()
    {
        int timerCount = Convert.ToInt32(_timerText.text);
        --timerCount;
        _timerText.text = timerCount.ToString();
    }
}