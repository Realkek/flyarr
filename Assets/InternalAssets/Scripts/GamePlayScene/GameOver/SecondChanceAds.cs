﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Events;


public class SecondChanceAds : MonoBehaviour, IEventSub, IEventTrigger
{
    [SerializeField] private EventsCollection secondChanceVideoClicked;
    [SerializeField] private EventsCollection secondChanceEarned;

    private RewardedAd rewardedAd;

    private const string adUnitId = "ca-app-pub-1542106522436399/8696417591";
    // private const string adUnitId = "ca-app-pub-3940256099942544/5224354917"; //test

    private void Start()
    {
        Subscribe(); 

        this.rewardedAd = new RewardedAd(adUnitId);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);

        // Called when an ad request has successfully loaded.
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;
    }

    void ShowAds()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
        }
    }

    private void CheckAdsIsLoaded()
    {
        if (rewardedAd.IsLoaded())
        {
            TriggerEvent("SecondChanceRewardAdsIsLoaded");
        }
        else
        {
            TriggerEvent("SecondChanceRewardAdsIsNotLoaded");
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("ArrowCollided", CheckAdsIsLoaded);
        StartListening(secondChanceVideoClicked.currentEvent, ShowAds);
    }

    public void UnSubscribe()
    {
        StopListening(secondChanceVideoClicked.currentEvent, ShowAds);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        TriggerEvent(secondChanceEarned.currentEvent);
    }
}