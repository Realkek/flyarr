﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlacementFigureData", menuName = "SO_DataStore/GamePlayData/PlacementFigureData")]
public class PlacementFigureData : ScriptableObject
{
    [SerializeField] private List<GameObject> collectingItems;

    [SerializeField] private EventsCollection placementFigureDataReceived;


    public string PlacementFigureDataReceived
    {
        get => placementFigureDataReceived.currentEvent;
    }

    public List<GameObject> GetCollectingItems
    {
        get => collectingItems;
        set => collectingItems = value;
    }
}