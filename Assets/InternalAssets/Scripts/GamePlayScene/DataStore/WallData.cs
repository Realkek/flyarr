﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс с данными стен, т.е их скины, скорость движения и т.д 
/// </summary>
[CreateAssetMenu(fileName = "WallData", menuName = "SO_DataStore/GamePlayData/WallData")]
public class WallData : ScriptableObject
{
    [SerializeField] private GameObject lrWallData;

    public GameObject LrwData => lrWallData;
}