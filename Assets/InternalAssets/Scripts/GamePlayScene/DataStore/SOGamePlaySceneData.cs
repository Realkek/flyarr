﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GamePlayData", menuName = "SO_DataStore/GamePlayData")]
public class SOGamePlaySceneData : ScriptableObject
{
    [SerializeField] private PlacementFigureData placementFigureData;

    [SerializeField] private EventsCollection gamePlayDataReceived;

    public string GamePlayDataReceived
    {
        get => gamePlayDataReceived.currentEvent;
    }

    public PlacementFigureData GetPlacementFigureData
    {
        get => placementFigureData;
    }
}