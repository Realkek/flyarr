﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TopAdventure;
using System;

public class GamePlaySceneData : MonoBehaviour, IEventTrigger
{
    public static GamePlaySceneData Instance { get; private set; }

    [SerializeField] private SOGamePlaySceneData data;

    private void Awake()
    {
        Instance = this;
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    public SOGamePlaySceneData GetSoGamePlayData => data;
}