﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LaserSquares : MonoBehaviour, IEventSub
{
    [SerializeField]
    private GameObject laserPositionsY;
    [SerializeField]
    List<GameObject> lasers = new List<GameObject>();
    [SerializeField]
    EventsCollection firstTeleportColiderReached;
    [SerializeField]
    EventsCollection secondTeleportColiderReached;
    private void OnEnable()
    {
        PlacementLasers();
        Subscribe();
    }
    void PlacementLasers()
    {
        ClearLasers();
        for (int childNumber = 0; childNumber < laserPositionsY.transform.childCount; childNumber++)
        {
            int laserNum = Random.Range(0, lasers.Count);
            CloneLaser(laserNum, childNumber);
        }
    }

    void FirstTeleport()
    {
        if (transform.parent.name == "LRWalls")
            PlacementLasers();
    }

    void SecondTeleport()
    {
        if (transform.parent.name != "LRWalls")
            PlacementLasers();
    }

    void CloneLaser(int laserNum, int childNumber)
    {
        GameObject laser = lasers[laserNum];
        GameObject clone = Instantiate(laser, laser.transform.position, Quaternion.identity) as GameObject;
        Transform figure;
        figure = clone.transform;
        Transform newLaserPos = laserPositionsY.transform.GetChild(childNumber);
        figure.SetParent(transform);
        figure.position = new Vector3(laser.transform.position.x, newLaserPos.position.y, newLaserPos.position.z);
    }

    void ClearLasers()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(firstTeleportColiderReached.currentEvent, FirstTeleport);
        StartListening(secondTeleportColiderReached.currentEvent, SecondTeleport);
    }

    public void UnSubscribe()
    {
        StopListening(firstTeleportColiderReached.currentEvent, FirstTeleport);
        StopListening(secondTeleportColiderReached.currentEvent, FirstTeleport);
    }
}
