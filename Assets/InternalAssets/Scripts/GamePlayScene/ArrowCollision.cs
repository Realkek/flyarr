﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCollision : MonoBehaviour, IEventTrigger
{
    [SerializeField] EventsCollection arrowCollided;
    [SerializeField] AudioClip explosionAudioClip;

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Item") == false)
        {
            TriggerEvent(arrowCollided.currentEvent);
            AudioPlayer.Instance.Play(explosionAudioClip);
        }
    }
}