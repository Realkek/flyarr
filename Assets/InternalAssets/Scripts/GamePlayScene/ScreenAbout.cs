﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenAbout : MonoBehaviour, IEventTrigger
{
    [SerializeField] protected EventsCollection screenDataState;

    private float _widthCam; // ширина камеры
    private Vector2 _centrCam; // центр камеры (это ее пивот)
    protected static float MinX, MaxX; // левая и правая границы


    private void Awake()
    {
        if (Camera.main != null)
        {
            var main = Camera.main;
            _widthCam = main.orthographicSize *
                        main.aspect; // Получаем половину ширины камеры, путем умножения высоты на соотношение
            _centrCam = main.transform
                .position; // получаем центр камеры, т.к. пивот у камеры по умолчанию в центре, центром будет ее позиция
        }

        MinX = _centrCam.x - _widthCam; // левый край  (отнимаем от центра половину ширины)
        MaxX = _centrCam.x + _widthCam; // правый край (прибавляем к центру половину ширины)
        TriggerEvent(screenDataState.currentEvent);
        Debug.Log("ScreenAboutAwaken");
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}