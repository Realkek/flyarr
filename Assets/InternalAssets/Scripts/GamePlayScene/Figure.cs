﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Figure : ScreenAbout, IEventSub
{
    protected float MoveSpeed;
    private static PlacementFigureData _placementFigureData;
    private static bool _isExistLrWallsFigure;
    private Ray _rightRay;
    private Ray _leftRay;
    protected float RayDistance = 100f;
    private RaycastHit _rightHit;
    private RaycastHit _leftHit;
    protected GameObject PlacedItem;
    private float _newFigurePosX;
    private float _randomItemPosX;

    private void Awake()
    {
        //Subscribe();
    }

    private void Start()
    {
        ReceivePlacementFigureData();
    }

    private void AlignmentRightRay(float x, float y, float z)
    {
        _rightRay.origin = new Vector3(x, y, z);
        _rightRay.direction = Vector3.left;
    }

    private void AlignmentLeftRay(float x, float y, float z)
    {
        _leftRay.origin = new Vector3(x, y, z);
        _leftRay.direction = Vector3.right;
    }

    protected void AlignmentFigure(Transform parentTransform, float minAdditionally, float maxAdditionally)
    {
        _newFigurePosX = Random.Range(MinX + minAdditionally, MaxX - maxAdditionally);
        parentTransform.position = new Vector2(_newFigurePosX, parentTransform.position.y);
    }

    private GameObject PlacementItem(List<GameObject> items, Transform parentFigure)
    {
        GameObject itemInstance = null;
        int _isPlace = Random.Range(0, 3);
        // int _isPlace = 1;
        if (_isPlace == 1 || _isPlace == 2)
        {
            int randomItem = Random.Range(0, items.Count);
            GameObject item = items[randomItem];
            itemInstance = Instantiate(item, parentFigure.transform.position, Quaternion.identity);
            itemInstance.transform.parent = parentFigure;
            _isExistLrWallsFigure = true;
        }

        return itemInstance;
    }

    private void PrepareItem(List<GameObject> items, Transform parentFigure)
    {
        PlacedItem = null;
        PlacedItem = PlacementItem(items, parentFigure);

        var newCollectingItemPosY = parentFigure.position.y;
        var randomCollectingItemPosY = Random.Range(0, 3);

        switch (randomCollectingItemPosY)
        {
            case 0:
                var position1 = parentFigure.position;
                var maxCollectingItemPosY = Random.Range(position1.y + 1f, position1.y + 2.5f);
                newCollectingItemPosY = maxCollectingItemPosY;
                break;
            case 1:
                var position2 = parentFigure.position;
                var minCollectingItemPosY = Random.Range(position2.y - 2.5f, position2.y - 1f);
                newCollectingItemPosY = minCollectingItemPosY;
                break;
            case 2:
                break;
        }

        if (PlacedItem != null)
        {
            var position = PlacedItem.transform.position;
            AlignmentRightRay(MaxX, newCollectingItemPosY, position.z);
            AlignmentLeftRay(MinX, newCollectingItemPosY, position.z);
            AligmentCollectingItemPos(PlacedItem, newCollectingItemPosY, _randomItemPosX, parentFigure);
            PlacedItem.transform.SetParent(parentFigure.transform.parent); 
        }
    }

    protected void GoPlacement(GameObject placedItem, Transform parentFigure)
    {
        if (placedItem == null && _placementFigureData != null)
            PrepareItem(_placementFigureData.GetCollectingItems, parentFigure);
    }

    public void AligmentCollectingItemPos(GameObject placedItem, float collectingItemPosY, float randomItemPosX,
        Transform parentTransform)
    {
        int side = Random.Range(0, 2);
        float newPosX;
        _randomItemPosX = Random.Range(MinX + 1f, MaxX - 1f);
        switch (side)
        {
            case 0:
                if (Physics.Raycast(_rightRay, out _rightHit))
                {
                    if (_rightHit.distance > 1.6f)
                    {
                        newPosX = Random.Range(_rightHit.point.x + 0.5f, MaxX - 1f);
                        placedItem.transform.position = new Vector3(newPosX, collectingItemPosY, transform.position.z);
                    }
                    else
                    {
                        placedItem.SetActive(false);
                    }
                }
                else if (collectingItemPosY != parentTransform.position.y)
                {
                    placedItem.transform.position =
                        new Vector3(randomItemPosX, collectingItemPosY, transform.position.z);
                }
                else
                {
                    placedItem.SetActive(false);
                }

                break;
            case 1:

                if (Physics.Raycast(_leftRay, out _leftHit))
                {
                    if (_leftHit.distance > 1.6f)
                    {
                        newPosX = Random.Range(MinX + 1f, _leftHit.point.x - 0.5f);
                        placedItem.transform.position = new Vector3(newPosX, collectingItemPosY, transform.position.z);
                    }
                    else
                    {
                        placedItem.SetActive(false);
                    }
                }
                else if (collectingItemPosY != parentTransform.position.y)
                {
                    placedItem.transform.position =
                        new Vector3(randomItemPosX, collectingItemPosY, transform.position.z);
                }
                else
                {
                    placedItem.SetActive(false);
                }

                break;
        }
    }

    private void ReceivePlacementFigureData()
    {
        _placementFigureData = GamePlaySceneData.Instance.GetSoGamePlayData.GetPlacementFigureData;
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening("GamePlayDataReceived", ReceivePlacementFigureData);
    }

    public void UnSubscribe()
    {
        StopListening("GamePlayDataReceived", ReceivePlacementFigureData);
    }
}