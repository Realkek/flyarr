﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ArrowVisibility : MonoBehaviour, IEventSub
{
    [SerializeField] EventsCollection arrowCollided;
    [SerializeField] private AudioClip startArrowAnim;
    [SerializeField] private EventsCollection secondChanceEarned;
    [SerializeField] public ArrowsContainer chosenArrowObjectList;
    SpriteRenderer _arrowSprite;
    Animation _anim;
    BoxCollider _arrowCollider;

    void Start()
    {
        foreach (var chosenArrowObject in chosenArrowObjectList.ArrowsDataContainer)
        {
            if (chosenArrowObject.Id == PlayerPrefs.GetString("ChosenArrowId"))
            {
                transform.GetComponent<SpriteRenderer>().sprite = chosenArrowObject.Icon;
                break;
            }
        }

        AudioPlayer.Instance.Play(startArrowAnim);
        _anim = GetComponentInParent<Animation>();
        _arrowCollider = GetComponent<BoxCollider>();
        Subscribe();
    }

    void HideArrow()
    {
        _arrowCollider.enabled = false;
        _anim.Play("HideArrow");
    }

    private void EnableBoxCollider()
    {
        _arrowCollider.enabled = true;
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(arrowCollided.currentEvent, HideArrow);
        StartListening(secondChanceEarned.currentEvent, EnableBoxCollider);
    }

    public void UnSubscribe()
    {
        StopListening(arrowCollided.currentEvent, HideArrow);
    }
}