﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Explosion : MonoBehaviour, IEventSub
{
    Animator _expAnimator;
    [SerializeField] EventsCollection arrowCollided;


    // Start is called before the first frame update
    void Start()
    {
        _expAnimator = GetComponent<Animator>();
        Subscribe();
    }

    void Explode()
    {
        _expAnimator.SetTrigger("explode");
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(arrowCollided.currentEvent, Explode);
    }

    public void UnSubscribe()
    {
        StopListening(arrowCollided.currentEvent, Explode);
    }
}