﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class WallMover : MonoBehaviour, ITick, IEventSub
{
    private float _moveSpeed = 3f;
    [SerializeField] private EventsCollection secondChanceEarned;
    [SerializeField] EventsCollection arrowCollided;
    [SerializeField] Score scoreField;
    private Vector3 _startWallPos;
    private int _previousScoreValue;
    private AudioSource _audioSource;
    private string _isSoundButtonState;

    private void Start()
    {
        _startWallPos = transform.position;
        GoPlay();
        Subscribe();
        _audioSource = GetComponent<AudioSource>();
        _isSoundButtonState = PlayerPrefs.GetString("isSoundButtonState");
        if (_isSoundButtonState == "ON")
        {
            _audioSource.mute = false;
        }
        else
        {
            _audioSource.mute = true;
        }
    }

    public void Subscribe()
    {
        StartListening(arrowCollided.currentEvent, StopPlay);
        StartListening(secondChanceEarned.currentEvent, GoPlay);
        StartListening(CollectedItemsEventsStore.MoveSpeedBuffReceived, IncreaseMovementSpeed);
    }

    private void IncreaseMovementSpeed()
    {
        StartCoroutine(MovementSpeedIncreaser());
    }

    private IEnumerator MovementSpeedIncreaser()
    {
        int i = 0;
        while (i < 15)
        {
            _moveSpeed += 0.2f;
            i++;
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(1.5f);
        _moveSpeed -= 3f;
    }

    void GoPlay()
    {
        transform.position = _startWallPos;
        ManagerUpdate.AddTo(this);
    }

    void StopPlay()
    {
        ManagerUpdate.RemoveFrom(this);
    }

    void Move()
    {
        transform.Translate(0, _moveSpeed * Time.deltaTime, 0);
    }

    public void Tick()
    {
        Move();
        ChangingMoveSpeed();
    }

    private void ChangingMoveSpeed()
    {
        if (scoreField.score > 0 && scoreField.score != _previousScoreValue && scoreField.score % 10 == 0)
        {
            if (scoreField.score <= 50)
                _moveSpeed += 0.2f;
            else if (scoreField.score <= 150)
                _moveSpeed += 0.1f;
            else if (scoreField.score <= 250)
                _moveSpeed += 0.05f;
            else
            {
                _moveSpeed += 0.025f;
            }

            // _audioSource.Play();
            _previousScoreValue = scoreField.score;
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }


    public void UnSubscribe()
    {
        StopListening(arrowCollided.currentEvent, StopPlay);
    }
}