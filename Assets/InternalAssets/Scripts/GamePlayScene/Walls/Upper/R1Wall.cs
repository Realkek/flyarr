﻿using UnityEngine;


public class R1Wall : L1R1, ITickFixed, IEventSub
{
    private void Awake()
    {
        Subscribe();
        ManagerUpdate.AddTo(this);
    }


    public void TickFixed()
    {
        var position = transform.position;
        float x = Mathf.Clamp(position.x - (_moveSpeed * Time.deltaTime), MaxX, MaxX + 0.50f);
        position = new Vector3(x, position.y, position.z);
        transform.position = position;
    }

    void UnUpdate()
    {
        ManagerUpdate.RemoveFrom(this);
    }

    void ScreenDataStateReceived()
    {
        var transform1 = transform;
        transform1.position = new Vector2(MaxX + 0.50f, transform1.position.y);
    }


    public new void Subscribe()
    {
        StartListening(screenDataState.currentEvent, ScreenDataStateReceived);
    }


    public new void UnSubscribe()
    {
        //StopListening(DestroyL1R1.currentEvent, UnUpdate);
        //StopListening(ScreenDataState.currentEvent, ScreenDataStateReceived);
    }
}