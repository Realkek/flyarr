﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class L1R1 : ScreenAbout, IEventTrigger, IEventSub
{
    [SerializeField] protected EventsCollection destroyL1R1;
    [SerializeField] private EventsCollection secondChanceEarned;

    protected float _moveSpeed = 0.3f; //скорость анимации выдвижения стены

    private bool _isTriggeredEvent;
    private Vector3 _startL1R1Pos;

    private void Awake()
    {
    }

    private void Start()
    {
        _startL1R1Pos = transform.localPosition;
        Subscribe();
        //GoMove();
    }

    private void GoMove()
    {
        ManagerUpdate.AddTo(this);
    }

    private void GoStartPos()
    {
        transform.localPosition = _startL1R1Pos;
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        //StartListening(DestroyL1R1.currentEvent, GoMove);
        StartListening(secondChanceEarned.currentEvent, GoStartPos);
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}