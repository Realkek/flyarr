﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class L1Wall : L1R1, ITickFixed, IEventSub
{
    private void Awake()
    {
        Subscribe();
        ManagerUpdate.AddTo(this);
    }

    public void TickFixed()
    {
        var position = transform.position;
        float x = Mathf.Clamp(position.x + (_moveSpeed * Time.deltaTime), MinX - 0.50f, MinX);
        position = new Vector3(x, position.y, position.z);
        transform.position = position;
    }

    void UnUpdate()
    {
        ManagerUpdate.RemoveFrom(this);
    }

    void ScreenDataStateReceived()
    {
        var transform1 = transform;
        transform1.position = new Vector2(MinX - 0.50f, transform1.position.y);
    }

    public new void Subscribe()
    {
        StartListening(screenDataState.currentEvent, ScreenDataStateReceived);
    }


    public new void UnSubscribe()
    {
        StopListening(destroyL1R1.currentEvent, UnUpdate);
        StopListening(screenDataState.currentEvent, ScreenDataStateReceived);
    }
}