﻿using UnityEngine;

public class ScoreColisioner : ScreenAbout, IEventTrigger
{
    [SerializeField] EventsCollection events;

    private void Awake()
    {
    }

    void Start()
    {
        var transform1 = transform;
        transform1.position = new Vector2(MinX - 0.5f, transform1.position.y);
    }

    private void OnTriggerEnter(Collider other)
    {
        TriggerEvent(events.currentEvent);
    }
}