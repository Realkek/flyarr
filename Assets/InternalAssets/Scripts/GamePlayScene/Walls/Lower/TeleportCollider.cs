﻿using System;
using UnityEngine;

public class TeleportCollider : MonoBehaviour, IEventTrigger
{
    [SerializeField] EventsCollection firstTeleportColliderReached;
    [SerializeField] EventsCollection secondTeleportColliderReached;

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }

    private void OnTriggerEnter(Collider other)
    {
        var nameWall = transform.parent.name;
        TriggerEvent(nameWall == "LRWalls"
            ? firstTeleportColliderReached.currentEvent
            : secondTeleportColliderReached.currentEvent);
    }
}