﻿using UnityEngine;
using UnityEngine.Events;

public class BothWalls : MonoBehaviour, ITickFixed, IEventSub
{
    [SerializeField] EventsCollection firstTeleportColiderReached;
    [SerializeField] EventsCollection secondTeleportColiderReached;
    [SerializeField] GameObject newWallPos;

    [SerializeField] private EventsCollection secondChanceEarned;

    private Vector3 _startPos;

    private void Start()
    {
        _startPos = transform.localPosition;
        ManagerUpdate.AddTo(this);
        Subscribe();
    }

    public void TickFixed()
    {
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(firstTeleportColiderReached.currentEvent, FirstTeleportColider);
        StartListening(secondTeleportColiderReached.currentEvent, SecondTeleportColider);
        StartListening(secondChanceEarned.currentEvent, GoStartPos);
    }

    private void GoStartPos()
    {
        transform.localPosition = _startPos;
    }

    void NewWallPosAdjustment()
    {
        var position = newWallPos.transform.position;
        position = new Vector3(position.x, transform.position.y - 40f,
            position.z);
        newWallPos.transform.position = position;
    }

    void FirstTeleportColider()
    {
        NewWallPosAdjustment();
        if (transform.name == "LRWalls")
            transform.position = newWallPos.transform.position;
    }

    void SecondTeleportColider()
    {
        NewWallPosAdjustment();
        if (transform.name != "LRWalls")
            transform.position = newWallPos.transform.position;
    }

    public void UnSubscribe()
    {
        throw new System.NotImplementedException();
    }
}