﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CrossHairRotator : MonoBehaviour
{
    private int _rotateDirection;

    private void Start()
    {
        _rotateDirection = Random.Range(0, 2);
    }

    private void Update()
    {
        if (_rotateDirection == 1)
            transform.Rotate(new Vector3(0, 0, 45) * Time.deltaTime);
        else if (_rotateDirection == 0)
        {
            transform.Rotate(new Vector3(0, 0, -45) * Time.deltaTime);
        }
    }
}