﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorseHoeHorizontal : Figure, IPlaceCollectingItems, IAlignmentFigure, IGamePlayDataAwaiter
{
    private const float MinAdditionallyFigurePosX = 1.6f;
    private const float MaxAdditionallyFigurePosX = 1.6f;

    private void Awake()
    {
    }

    void Start()
    {
        AlignmentCurrentFigure();
        StartCoroutine(Wait());
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.4f);
        PlacementCurrentCollectingItem();
    }

    public void AlignmentCurrentFigure()
    {
        AlignmentFigure(transform, MinAdditionallyFigurePosX, MaxAdditionallyFigurePosX);
    }

    public void PlacementCurrentCollectingItem()
    {
        GoPlacement(PlacedItem, transform);
    }
}