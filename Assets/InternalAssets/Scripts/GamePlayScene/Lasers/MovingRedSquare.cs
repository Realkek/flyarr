﻿using UnityEngine;

public class MovingRedSquare : Figure, ITick
{
    private float moveSpeed = 1.8f;
    private bool _isRight;

    private void Awake()
    {
        ManagerUpdate.AddTo(this);
    }

    private void Start()
    {
        if (transform.position.x >= MaxX - 0.741f)
            _isRight = false;
        else if (transform.position.x <= MinX + 0.741f)
            _isRight = true;
    }

    void Move()
    {
        if (_isRight)
            MoveToRight();
        else
            MoveToLeft();
    }


    void MoveToRight()
    {
        if (transform.position.x >= MaxX - 0.741f)
        {
            _isRight = false;
            return;
        }

        transform.Translate(moveSpeed * Time.deltaTime, 0, 0);
    }

    void MoveToLeft()
    {
        if (transform.position.x <= MinX + 0.8f)
        {
            _isRight = true;
            return;
        }

        transform.Translate(-moveSpeed * Time.deltaTime, 0, 0);
    }


    public void Tick()
    {
        Move();
    }

    private void OnDestroy()
    {
        try
        {
            ManagerUpdate.RemoveFrom(this);
        }
        catch
        {
        }
    }
}