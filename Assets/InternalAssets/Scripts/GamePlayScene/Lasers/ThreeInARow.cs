﻿using System.Collections;
using UnityEngine;

public class ThreeInARow : Figure, IPlaceCollectingItems, IAlignmentFigure, IGamePlayDataAwaiter
{
    private const float MinAdditionallyFigurePosX = 1.4f;
    private const float MaxAdditionallyFigurePosX = 2.8f;

    private void Awake()
    {
    }

    void Start()
    {
        AlignmentCurrentFigure();
        StartCoroutine(Wait());
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.4f);
        PlacementCurrentCollectingItem();
    }

    public void AlignmentCurrentFigure()
    {
        AlignmentFigure(transform, MinAdditionallyFigurePosX, MaxAdditionallyFigurePosX);
    }

    public void PlacementCurrentCollectingItem()
    {
        GoPlacement(PlacedItem, transform);
    }
}