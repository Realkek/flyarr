﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombVisibility : MonoBehaviour
{
    [SerializeField] private ArrowData chosenBomb;
    [SerializeField] private ArrowsContainer chosenBombObjectList;

    private void Start()
    {
        foreach (var chosenArrowObject in chosenBombObjectList.ArrowsDataContainer)
        {
            if (chosenArrowObject.Id == PlayerPrefs.GetString("ChosenBombId"))
            {
                transform.GetComponent<SpriteRenderer>().sprite = chosenArrowObject.Icon;
                break;
            }
        }
    }
}