﻿using UnityEngine;

public class DetectClicks : MonoBehaviour, IEventTrigger
{
    [SerializeField] private EventsCollection tap;

    private void OnMouseDown()
    {
        TriggerEvent(tap.currentEvent);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}