﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour, IEventTrigger
{
    [SerializeField] private EventsCollection gemCollided;

    private void OnTriggerEnter(Collider other)
    {
        ActivateGem(other);
    }

    private void ActivateGem(Collider whoIsCollidedObject)
    {
        if (whoIsCollidedObject.CompareTag("Player"))
        {
            TriggerEvent(gemCollided.currentEvent);
            gameObject.SetActive(false);
        }
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}