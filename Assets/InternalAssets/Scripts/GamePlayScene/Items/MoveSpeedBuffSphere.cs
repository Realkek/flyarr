﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedBuffSphere : MonoBehaviour, IEventTrigger
{
    [SerializeField] private AudioClip increaseMovementSpeedSound;
    [SerializeField] private AudioClip bubblePopSound;
    private AudioSource _audioSource;
    private string _isSoundButtonState;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _isSoundButtonState = PlayerPrefs.GetString("isSoundButtonState");
        if (_isSoundButtonState == "ON")
        {
            _audioSource.mute = false;
        }
        else
        {
            _audioSource.mute = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ActivateGem(other);
    }

    private void ActivateGem(Collider whoIsCollidedObject)
    {
        if (whoIsCollidedObject.CompareTag("Player"))
        {
            _audioSource.PlayOneShot(bubblePopSound);
            StartCoroutine(MovementSpeedIncreaseSounder());
            TriggerEvent(CollectedItemsEventsStore.MoveSpeedBuffReceived);
            transform.GetChild(0).GetComponent<ParticleSystem>().Play();
            StartCoroutine(ItemDisabler());
        }
    }

    private IEnumerator MovementSpeedIncreaseSounder()
    {
        yield return new WaitForSeconds(0.3f);
        _audioSource.PlayOneShot(increaseMovementSpeedSound);
    }

    private IEnumerator ItemDisabler()
    {
        GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(2);
        gameObject.SetActive(false);
    }

    public void TriggerEvent(string eventName)
    {
        ManagerEvents.TriggerEvent(eventName);
    }
}