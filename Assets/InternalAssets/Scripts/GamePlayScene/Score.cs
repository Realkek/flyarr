﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Score : MonoBehaviour, IEventSub
{
    [SerializeField] private EventsCollection events;
    public int score;

    private void OnEnable()
    {
        Subscribe();
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        Toolbox.Get<ManagerUpdate>();
        ManagerEvents.StartListening(eventName, listener);
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener);
    }

    public void Subscribe()
    {
        StartListening(events.currentEvent, ScoreChanged);
    }

    public void UnSubscribe()
    {
        StopListening(events.currentEvent, ScoreChanged);
    }

    private void ScoreChanged()
    {
        score++;
        transform.GetComponent<Text>().text = score.ToString();
    }
}