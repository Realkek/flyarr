﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowView : MonoBehaviour
{
    [SerializeField] private ArrowsContainer chosenArrowObjectList;

    private void Start()
    {
        foreach (var chosenArrowObject in chosenArrowObjectList.ArrowsDataContainer)
        {
            if (chosenArrowObject.Id == PlayerPrefs.GetString("ChosenArrowId"))
            {
                transform.GetComponent<SpriteRenderer>().sprite = chosenArrowObject.Icon;
                break;
            }
        }
    }
}