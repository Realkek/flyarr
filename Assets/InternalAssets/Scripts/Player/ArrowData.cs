﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ArrowData", menuName = "Arrow Data", order = 51)]
public class ArrowData : ScriptableObject
{
    // Start is called before the first frame update
    [SerializeField] private string itemName;
    [SerializeField] private string id;
    [SerializeField] private string description;
    [SerializeField] private Sprite sprite;
    [SerializeField] private int goldCost;
    [SerializeField] private float moveSpeed;
    [SerializeField] private int health;

    public string ItemName
    {
        get => itemName;
        set => itemName = value;
    }

    public string Id
    {
        get => id;
        set => id = value;
    }

    public string Description => description;

    public Sprite Icon
    {
        get => sprite;
        set => sprite = value;
    }


    public int GoldCost => goldCost;

    public float MoveSpeed => moveSpeed;
}