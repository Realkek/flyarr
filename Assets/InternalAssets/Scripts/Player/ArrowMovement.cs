﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ArrowMovement : MonoBehaviour, ITick, IEventSub
{
    [SerializeField] private ArrowData arrowData;
    private int _isMoveRight;
    [SerializeField] EventsCollection arrowCollided;
    [SerializeField] private EventsCollection tap;
    [SerializeField] private EventsCollection _secondChanceEarned;
    [SerializeField] AudioClip changeDirectionAudioClip;
    private AudioSource _audioSource;
    [SerializeField] Score scoreField;
    private Vector3 _startArrowPos;
    private Animation _anim;
    private int _previousScoreValue;
    private float _moveSpeed = 2.1f;
    private string _isSoundButtonState;

    private void Start()
    {
        _startArrowPos = transform.localPosition;
        _anim = transform.GetComponent<Animation>();
        AnimNameChoose();
        _audioSource = GetComponent<AudioSource>();
        _isSoundButtonState = PlayerPrefs.GetString("isSoundButtonState");
        if (_isSoundButtonState == "ON")
        {
            _audioSource.mute = false;
        }
        else
        {
            _audioSource.mute = true;
        }
    }

    public void Subscribe()
    {
        StartListening(tap.currentEvent, ChangeDirection);
        StartListening(arrowCollided.currentEvent, StopPlay);
        StartListening(_secondChanceEarned.currentEvent, SecondChanseUnmute);
        StartListening(_secondChanceEarned.currentEvent, AnimNameChoose);
        StartListening(CollectedItemsEventsStore.MoveSpeedBuffReceived, IncreaseMovementSpeed);
    }

    private void IncreaseMovementSpeed()
    {
        StartCoroutine(MovementSpeedIncreaser());
    }

    private IEnumerator MovementSpeedIncreaser()
    {
        int i = 0;
        while (i < 15)
        {
            _moveSpeed += 0.1f;
            i++;
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(1.5f);
        _moveSpeed -= 1.5f;
    }


    void AnimNameChoose()
    {
        transform.localPosition = _startArrowPos;
        string animName;
        int randAnim = Random.Range(0, 2);
        if (randAnim.Equals(1))
        {
            animName = "ShowArrow_Right";
        }
        else
        {
            animName = "ShowArrow_Left";
        }

        _anim.Play(animName);
    }

    void ChangeDirection()
    {
        if (_isMoveRight.Equals(1))
        {
            _isMoveRight = 0;
            transform.GetChild(0).Rotate(0, 0, -60.0f);
        }
        else if (_isMoveRight.Equals(0))
        {
            _isMoveRight = 1;
            transform.GetChild(0).Rotate(0, 0, 60.0f);
        }

        _audioSource.PlayOneShot(changeDirectionAudioClip);
    }

    private void GoPlay(int isRight)
    {
        UnSubscribe();
        Subscribe();
        ManagerUpdate.AddTo(this);
        _isMoveRight = isRight;
    }

    public void Tick()
    {
        ChangingMoveSpeed();
        Move();
    }

    void Move()
    {
        switch (_isMoveRight)
        {
            case 0:
                transform.Translate(-_moveSpeed * Time.deltaTime, 0, 0);
                break;
            case 1:
                transform.Translate(_moveSpeed * Time.deltaTime, 0, 0);
                break;
        }
    }

    private void ChangingMoveSpeed()
    {
        if (scoreField.score > 0 && scoreField.score != _previousScoreValue && scoreField.score % 10 == 0)
        {
            if (scoreField.score <= 50)
                _moveSpeed += 0.025f;
            else if (scoreField.score <= 150)
                _moveSpeed += 0.125f;
            else if (scoreField.score <= 250)
                _moveSpeed += 0.0625f;
            else
            {
                _moveSpeed += 0.03125f;
            }

            _previousScoreValue = scoreField.score;
        }
    }

    public void StartListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StartListening(eventName, listener); //передаем название события и метод-обработчик
    }

    public void StopListening(string eventName, UnityAction listener)
    {
        ManagerEvents.StopListening(eventName, listener); //передаем название события и метод-обработчик
    }

    private void StopPlay()
    {
        _audioSource.mute = true;
        ManagerUpdate.RemoveFrom(this);
    }


    private void SecondChanseUnmute()
    {
        _audioSource.mute = false;
    }

    public void UnSubscribe()
    {
        StopListening(tap.currentEvent, ChangeDirection);
        StopListening(arrowCollided.currentEvent, StopPlay);
        StopListening(_secondChanceEarned.currentEvent, SecondChanseUnmute);
        StopListening(_secondChanceEarned.currentEvent, AnimNameChoose);
    }
}